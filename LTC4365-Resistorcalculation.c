// Include needed header files
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

// Define I_UV = 10nA
#define         I_UV            (1e-8)

// List with all E96 resistor values
const double E96_RESISTOR_VALUES[] = {
	1.00, 1.02, 1.05, 1.07, 1.10, 1.13, 1.15, 1.18, 
	1.21, 1.24, 1.27, 1.30, 1.33, 1.37, 1.40, 1.43,
	1.47, 1.50, 1.54, 1.58, 1.62, 1.65, 1.69, 1.74, 
	1.78, 1.82, 1.87, 1.91, 1.96, 2.00, 2.05, 2.10, 
	2.15, 2.21, 2.26, 2.32, 2.37, 2.43, 2.49, 2.55, 
	2.61, 2.67, 2.74, 2.80, 2.87, 2.94, 3.01, 3.09, 
	3.16, 3.24, 3.32, 3.40, 3.48 ,3.57, 3.65, 3.74, 
	3.83, 3.92, 4.02, 4.12, 4.22, 4.32, 4.42, 4.53, 
	4.64, 4.75, 4.87, 4.99, 5.11, 5.23, 5.36, 5.49, 
	5.62, 5.76, 5.90, 6.04, 6.19, 6.34, 6.49, 6.65, 
	6.81, 6.98, 7.15, 7.32, 7.50, 7.68, 7.87, 8.06, 
	8.25, 8.45, 8.66, 8.87, 9.09, 9.31, 9.53, 9.76
};

// Calculate R1 according to the datasheet
double calc_r1(const double v_os, const double ov_th, const double r3) {
  return (((v_os / I_UV) + r3) / ov_th) * 0.5;
}
// Calculate R2 according to the datasheet
double calc_r2(const double v_os, const double r1) {
  return (v_os / I_UV) - r1;
}

// Calculate R3 according to the datasheet
double calc_r3(const double v_os, const double uv_th) {
  return (v_os / I_UV) * ((uv_th - 0.5) / 0.5);
}

// Get E96 values for any resistor
double get_e96_resistor(double value, const bool gt) {
  int divided = 0;
  while (value > 10) {
    value /= 10;
    divided++;
  }

  int selected_index;
  for(int i = 0; i < 96 && value > E96_RESISTOR_VALUES[i]; i++) {
    selected_index = i;
  }

  if(gt) {     
    selected_index++;
  }

  if(selected_index > 95) {
    selected_index %= 96;
    divided++;
  }

  double selected_resistor = E96_RESISTOR_VALUES[selected_index] * (int) (pow(10, divided));
  return selected_resistor;
}
// Calculate UV_TH for a given resistor R3
double calc_uv_th(const double v_os, const double r3) {
  return ((r3 * I_UV) / (2*v_os)) + 0.5;
}

// Calculate =V_TH for given resistors R1, R3
double calc_ov_th(const double v_os, const double r1, const double r3) {
  return (((v_os / I_UV) + r3) / (2*r1));
}

// Transform resistors to readable strings
void str_rres(double r_val, char* str_p) {
  if(r_val > 1000000) {
    sprintf(str_p, "%.2fM", (r_val/1000000));
  } else if(r_val > 1000) {
    sprintf(str_p, "%.2fk", (r_val/1000));
  } else {
    sprintf(str_p, "%.2f", r_val);
  }
}

// Calculate possible resistors
void calc_resistor_possibilities(const double v_os, const double uv_th, const double ov_th) {
  bool r1_gt[] = {false, true, false, true};
  bool r3_gt[] = {false, false, true, true};
  double r1, r2, r3;
  char buf_r1[64], buf_r2[64], buf_r3[64];
    
  for(int i = 0; i < sizeof(r1_gt); i++) {
    r3 = get_e96_resistor(calc_r3(v_os, uv_th), r3_gt[i]);
    r1 = get_e96_resistor(calc_r1(v_os, ov_th, r3), r1_gt[i]);
    r2 = get_e96_resistor(calc_r2(v_os, r1), true);


    str_rres(r1, buf_r1);
    str_rres(r2, buf_r2);
    str_rres(r3, buf_r3);
    printf("[POSSIBLE RESULT] UV_TH=%.2fV\t\tOV_TH=%.2fV\t\tR1=%s\t\tR2=%s\t\tR3=%s\n",
           calc_uv_th(v_os, r3), calc_ov_th(v_os, r1, r3), buf_r1, buf_r2, buf_r3);
  }
}

// Program start
int main(int argc, char *argv[]) {

  // Check if amount of arguments is correct.
  // The arguments also contain the program name
  if(argc != 4) {
    printf("Usage: ./%s V_OS UV_TH OV_TH", argv[0]);
    return EXIT_FAILURE;
  }

  // Ignore program name
  argc--; argv++;

  // Parse values
  const double v_os   = atof(argv[0]);
  const double uv_th  = atof(argv[1]);
  const double ov_th  = atof(argv[2]);

  printf("[INFO] Calculating for V_OS(UV) = %fV and voltage range of %fV...%fV\n",
         v_os, uv_th, ov_th);

  // Call function to determine all possibilities
  calc_resistor_possibilities(v_os, uv_th, ov_th);

  return EXIT_SUCCESS;
}


