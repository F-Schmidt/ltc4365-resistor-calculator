# ltc4365-resistor-calculator

This repository implements a simple resistor calculator for over-/under-/reverse voltage protection IC LTC4365

Falko Schmidt, Uni Ulm, 2021